﻿using GammaShop.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GammaShop.Controllers
{
    public class ReviewController : Controller
    {
        private ApplicationDbContext db = ApplicationDbContext.Create();

        // GET: Review
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendReview(Review review)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    review.AuthorId = User.Identity.GetUserId();
                    db.Reviews.Add(review);
                    db.SaveChanges();
                    TempData["message"] = "Review was added!";

                }
            }
            catch (Exception e)
            {
                
            }

            return RedirectToAction("Show", "Product", new
            {
                id = review.ProductId
            });
        }

        // GET: vrem sa afisam formularul de edit
        //[Authorize(Roles = "Collaborator,Administrator")]
        public ActionResult Edit(int id)
        {
            Review review = db.Reviews.Find(id);

            if (review.AuthorId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                return View(review);
            }
            else
            {
                TempData["message"] = "You do not have permission to edit a product that does not belong to you!";

                return RedirectToAction("Show", "Product", new
                {
                    id = review.ProductId
                });
            }
        }

        // PUT: vrem sa trimitem modificarile la server si sa le salvam
        [HttpPut]
        //[Authorize(Roles = "Collaborator, Administrator")]
        public ActionResult Edit(int id, Review requestReview)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Review review = db.Reviews.Find(id);

                    if (review.AuthorId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
                    {
                        if (TryUpdateModel(review))
                        {
                            review.Title = requestReview.Title;
                            review.Comment = requestReview.Comment;
                            review.Rating = requestReview.Rating;
                            db.SaveChanges();
                            TempData["message"] = "Review was modified!";
                        }

                        return RedirectToAction("Show", "Product", new
                        {
                            id = review.ProductId
                        });
                    }
                    else
                    {
                        TempData["message"] = "You do not have permission to edit a review that does not belong to you!";

                        return RedirectToAction("Show", "Product", new
                        {
                            id = review.ProductId
                        });
                    }

                }
                else
                {
                    return View(requestReview);
                }
            }
            catch (Exception e)
            {
                return View(requestReview);
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            Review review = db.Reviews.Find(id);
            if (review.AuthorId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                db.Reviews.Remove(review);
                db.SaveChanges();
                TempData["message"] = "Review was removed!";

                return RedirectToAction("Show", "Product", new
                {
                    id =
                 review.ProductId
                });
            }
            else
            {
                TempData["message"] = "You do not have permission to delete a review that does not belong to you!";

                return RedirectToAction("Show", "Product", new
                {
                    id =
                review.ProductId
                });
            }

        }
    }
}