﻿using GammaShop.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace GammaShop.Controllers
{
    public class ProductController : Controller
    {
        private ApplicationDbContext db = ApplicationDbContext.Create();
        private int _perPage = 6;

        public ActionResult Index(string sortOrder, string searchString, string clearFilters)
        {
            var productsBrute = db.Products.Include("Category").Include("User");
            IOrderedQueryable<Product> products;

            var query = from product in productsBrute
                        where product.Approved == true
                        select product;

            products = FilterProducts(query, sortOrder, searchString, clearFilters);

            var totalItems = products.Count();
            var currentPage = Convert.ToInt32(Request.Params.Get("page"));

            var paginatedProducts = PaginateProduct(products, currentPage);


            ViewBag.perPage = this._perPage;
            ViewBag.total = totalItems;
            ViewBag.lastPage = Math.Ceiling((float)totalItems / (float)this._perPage);
            ViewBag.Products = paginatedProducts;
            //ViewBag.Categories = GetAllCategories();

            return View();
        }

        public ActionResult Show(int id)
        {
            Product product = db.Products.Find(id);

            var reviews = db.Reviews.Where(r => r.ProductId == id);
            var procentRatingArray = new float[5];
            var numberEachRating = new int[5];
            if (reviews.Count() != 0)
            {
                float sum = reviews.Sum(r => r.Rating);
                var totalReviews = reviews.Count();
                product.Rating = (float)Math.Round((sum / totalReviews), 2);
                for (int i = 0; i < 5; i++)
                {
                    var ratingsNumber = reviews.Where(r => r.Rating == (i + 1)).Count();
                    procentRatingArray[i] = ((float)ratingsNumber / totalReviews) * 100;
                    numberEachRating[i] = ratingsNumber;
                }
            }
            else
            {
                product.Rating = 0;
                for (int i = 0; i < 5; i++)
                {
                    procentRatingArray[i] = 0;
                    numberEachRating[i] = 0;
                }
            }

            db.SaveChanges();
            ViewBag.product = product;

            ViewData["procentRating"] = procentRatingArray;
            ViewData["numberEachRating"] = numberEachRating;

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }

            var review = new Review()
            {
                ProductId = product.Id
            };
            ViewBag.afisareButoane = false;
            if (User.IsInRole("Collaborator") || User.IsInRole("Administrator"))
            {
                ViewBag.showButtons = true;
            }
            ViewBag.isAdmin = User.IsInRole("Administrator");
            ViewBag.currentUser = User.Identity.GetUserId();

            return View("Show", review);
        }

        //GET: show form for new product
        [Authorize(Roles = "Collaborator, Administrator")]
        public ActionResult New()
        {
            Product product = new Product();
            // preluam lista de categorii din metoda GetAllCategories()
            product.Categories = GetAllCategories();

            product.UserId = User.Identity.GetUserId();

            return View(product);
        }

        [NonAction]
        public IEnumerable<SelectListItem> GetAllCategories()
        {
            // generam o lista goala
            var selectList = new List<SelectListItem>();
            // Extragem toate categoriile din baza de date
            var categories = from cat in db.Categories
                             select cat;
            // iteram prin categorii
            foreach (var category in categories)
            {
                // Adaugam in lista elementele necesare pentru dropdown
                selectList.Add(new SelectListItem
                {
                    Value = category.Id.ToString(),
                    Text = category.Title.ToString()
                });
            }

            // returnam lista de categorii
            return selectList;
        }

        //POST: trimitem datele produsului catre server pt creare
        [HttpPost]
        [Authorize(Roles = "Collaborator, Administrator")]
        [ValidateInput(false)]
        public ActionResult New(Product product)
        {
            product.Categories = GetAllCategories();
            try
            {
                if (ModelState.IsValid)
                {
                    // Protect content from XSS
                    product.Description = Sanitizer.GetSafeHtmlFragment(product.Description);

                    string fileName = Guid.NewGuid().ToString().Replace("-", "") + "_" + Path.GetFileName(product.Picture.FileName);
                    product.PicturePath = "~/Content/Images/Product/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Content/Images/Product/"), fileName);
                    product.Picture.SaveAs(fileName);
                    TempData["message"] = "Product was sent to pending list";

                    if (User.IsInRole("Administrator"))
                    {
                        product.Approved = true;
                        TempData["message"] = "Product was added!";
                    }

                    db.Products.Add(product);
                    db.SaveChanges();

                    ModelState.Clear();

                    return RedirectToAction("Index");
                }
                else
                {
                    return View(product);
                }

            }
            catch (Exception e)
            {
                return View();
            }
        }

        // GET: vrem sa afisam formularul de edit
        [Authorize(Roles = "Collaborator,Administrator")]
        public ActionResult Edit(int id)
        {
            Product product = db.Products.Find(id);

            product.Categories = GetAllCategories();
            if (product.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                return View(product);
            }
            else
            {
                TempData["message"] = "You do not have permission to edit a product that does not belong to you!";

                return RedirectToAction("Index");
            }
        }

        // PUT: vrem sa trimitem modificarile la server si sa le salvam
        [HttpPut]
        [Authorize(Roles = "Collaborator, Administrator")]
        [ValidateInput(false)]
        public ActionResult Edit(int id, Product requestProduct)
        {
            requestProduct.Categories = GetAllCategories();
            try
            {
                if (ModelState.IsValid)
                {
                    Product product = db.Products.Find(id);

                    if (product.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
                    {
                        if (TryUpdateModel(product))
                        {
                            product.Title = requestProduct.Title;

                            // Protect content from XSS
                            product.Description = Sanitizer.GetSafeHtmlFragment(product.Description);
                            product.Description = requestProduct.Description;
                            product.Price = requestProduct.Price;
                            product.ListingDate = requestProduct.ListingDate;
                            product.CategoryId = requestProduct.CategoryId;
                            product.Manufacturer = requestProduct.Manufacturer;
                            db.SaveChanges();
                            TempData["message"] = "Product was modified!";
                        }

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["message"] = "You do not have permission to edit a product that does not belong to you!";
                        return RedirectToAction("Index");
                    }

                }
                else
                {
                    return View(requestProduct);
                }
            }
            catch (Exception e)
            {
                return View(requestProduct);
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            Product product = db.Products.Find(id);
            if (product.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                string path = Server.MapPath("~/Content/Images/Product/" + product.PicturePath);
                FileInfo productImage = new FileInfo(path);
                if (productImage.Exists)
                {
                    productImage.Delete();
                }
                db.Products.Remove(product);
                db.SaveChanges();
                TempData["message"] = "Product was removed!";

                return RedirectToAction("Index");
            }
            else
            {
                TempData["message"] = "You do not have permission to delete a product that does not belong to you!";
                return RedirectToAction("Index");
            }
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Pendings(string sortOrder, string searchString, string clearFilters)
        {
            var productsBrute = db.Products.Include("Category").Include("User");
            IOrderedQueryable<Product> products;

            var query = from product in productsBrute
                        where product.Approved == false
                        select product;

            products = FilterProducts(query, sortOrder, searchString, clearFilters);

            ViewBag.Products = (IQueryable <Product>)products;
            return View();
        }

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        [ValidateInput(false)]
        public ActionResult Pendings(int id)
        {
            try
            {
                Product product = db.Products.Find(id);
                product.Approved = true;
                db.SaveChanges();

                return RedirectToAction("Pendings", "Product", new { clearFilters = true});
            }
            catch (Exception e)
            {
                return View();
            }
        }

        [NonAction]
        public IOrderedQueryable<Product> FilterProducts(IQueryable<Product> query, string sortOrder, string searchString, string clearFilters)
        {
            IOrderedQueryable<Product> products;

            if (!String.IsNullOrEmpty(clearFilters))
            {
                TempData["search"] = null;
            }
            else
            {
                if (String.IsNullOrEmpty(searchString))
                {
                    if (!String.IsNullOrEmpty((String)TempData["search"]))
                    {
                        searchString = (String)TempData["search"];
                    }
                }
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                query = query.Where(p => p.Title.Contains(searchString));
                TempData["search"] = searchString;
                TempData.Keep("search");
            }

            switch (sortOrder)
            {
                case "date_desc":
                    products = query.OrderByDescending(p => p.ListingDate);
                    break;

                case "rating_asc":
                    products = query.OrderBy(p => p.Rating);
                    break;

                case "rating_desc":
                    products = query.OrderByDescending(p => p.Rating);
                    break;

                default:
                    products = query.OrderBy(p => p.ListingDate);
                    break;
            }

            return products;
        }

        [NonAction]
        public IQueryable<Product> PaginateProduct(IOrderedQueryable<Product> products, int currentPage)
        {
            var offset = 0;

            if (!currentPage.Equals(0))
            {
                offset = (currentPage - 1) * this._perPage;
            }

            var paginatedProducts = products.Skip(offset).Take(this._perPage);

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }

            return paginatedProducts;
        }
    }
}

