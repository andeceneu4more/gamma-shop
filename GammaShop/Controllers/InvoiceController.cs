﻿using GammaShop.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GammaShop.Controllers
{
    public class InvoiceController : Controller
    {
        private ApplicationDbContext db = ApplicationDbContext.Create();
        private Invoice _invoice;
        private Boolean _created;

        // GET: istoricul comenzilor
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();

            var invoicesBrute = db.Invoices.Include("User");

            var invoices = from invoice in invoicesBrute
                           where invoice.Finished == true
                           where invoice.UserId == userId
                           select invoice;

            ViewBag.Invoices = invoices;

            return View();
        }

        public ActionResult Show(int id)
        {
            Invoice invoice = db.Invoices.Find(id);

            ViewBag.Total = invoice.Bill;

            var orders = GetOrderByInvoice(invoice);

            ViewBag.Orders = orders.Select(o => o).ToList();

            return View();
        }

        // GET: show lat unfinished invoice if exists
        // Index cu produsele din cos
        public ActionResult Cart()
        {
            if (!Request.IsAuthenticated)
            {
                TempData["message"] = "Before you go shopping you must be authenticated!";
                return RedirectToAction("Login", "Account");
            }

            _invoice = GetCurrentInvoice();
            var orders = GetOrderByInvoice(_invoice);

            if (_created || !orders.Any())
            {
                TempData["message"] = "The shopping cart is empty!";
            }
            else
            {
                // compute the bill
                ViewBag.Invoice = _invoice;
                ViewBag.Orders = orders.Select(o => o).ToList();

                float total = 0;

                foreach (var order in ViewBag.Orders)
                {
                    total = total + (order.Quantity * order.Product.Price);
                }

                ViewBag.Total = total;
                _invoice.Bill = total;
                db.SaveChanges();
            }

            return View();
        }

        [HttpPost]
        public ActionResult UpdateQuantity(int orderId, int productId, int quantity)
        {
            var order = db.Orders.FirstOrDefault(c => c.Id == orderId);
            order.Quantity = quantity;

            db.SaveChanges();

            return RedirectToAction("Cart", routeValues: new { id = order.Id });
        }

        [HttpPost]
        public ActionResult Add(int id, String quantity)
        {
            if (!Request.IsAuthenticated)
            {
                TempData["message"] = "Before you go shopping you must be authenticated!";
                return RedirectToAction("Login", "Account");
            }

            int quantityNum = 1;
            if (!String.IsNullOrEmpty(quantity))
            {
                quantityNum = Int32.Parse(quantity);
            }

            var product = db.Products.Find(id);
            _invoice = GetCurrentInvoice();

            if (_created)
            {
                db.Invoices.Add(_invoice);
            }
            db.SaveChanges();

            var orders = GetOrderByInvoice(_invoice);

            var query = orders.Where(o => o.Product.Id == product.Id);
            if (query.Any())
            {
                TempData["message"] = "Product is already added to cart";
            }
            else
            {
                // new order if doesn't exist
                Order order = new Order();
                order.InvoiceId = _invoice.Id;
                order.ProductId = product.Id;
                order.UnitPrice = product.Price;
                order.Quantity = quantityNum;
                order.RequestingDate = DateTime.Now;

                db.Orders.Add(order);
                db.SaveChanges();

                TempData["message"] = "Product has been added to cart";
            }

            return RedirectToAction("Index", "Product");
        }

        public ActionResult Finish(int id)
        {
            if (!Request.IsAuthenticated)
            {
                TempData["message"] = "Before you go shopping you must be authenticated!";
                return RedirectToAction("Login", "Account");
            }

            var invoice = db.Invoices.Find(id);
            var userId = User.Identity.GetUserId();

            if (userId != invoice.UserId)
            {
                TempData["message"] = "You don't have permission to finish that order!";
                return RedirectToAction("Login", "Account");
            }

            invoice.Finished = true;
            db.SaveChanges();

            TempData["message"] = "The order it's done, for more details, please check your e-mail.";

            return RedirectToAction("Index", "Product");
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            var order = db.Orders.Find(id);

            // update the bill
            var product = order.Product;
            _invoice = GetCurrentInvoice();

            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Cart");
        }

        [NonAction]
        private Invoice GetCurrentInvoice()
        {
            var userId = User.Identity.GetUserId();
            _created = false;

            if (_invoice == null || (_invoice != null && _invoice.UserId == userId))
            {
                var invoicesBrute = db.Invoices.Include("User");

                var invoices = from invoice in invoicesBrute
                                where invoice.Finished == false
                                where invoice.UserId == userId
                                select invoice;

                if (invoices.Any())
                {
                    _invoice = invoices.First();
                }
                else
                {
                    _invoice = new Invoice();
                    _invoice.UserId = userId;
                    _invoice.RequestingDate = DateTime.Now;
                    _created = true;
                }
            }
            return _invoice;
        }

        [NonAction]
        public IQueryable<Order> GetOrderByInvoice(Invoice invoice)
        {
            var ordersBrute = db.Orders.Include("Invoice").Include("Product");
            var orders = from order in ordersBrute
                         where order.InvoiceId == invoice.Id
                         select order;

            return orders;
        }
    }
}