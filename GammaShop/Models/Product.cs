﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Web.Mvc;

namespace GammaShop.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [StringLength(20, ErrorMessage = "Title length should be less than 20 characters")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [StringLength(300, ErrorMessage = "Description length should be less than 300 characters")]
        public string Description { get; set; }

        // to be positive
        [Required(ErrorMessage = "Price is required")]
        public float Price { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "The field should be a data type")]
        public DateTime ListingDate { get; set; }

        [Required(ErrorMessage = "Category is required")]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }

        [Required(ErrorMessage = "Manufacturer is required")]
        public string Manufacturer { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }

        [DisplayName("Upload Picture")]
        public string PicturePath { get; set; }

        public float Rating { get; set; } = 0;

        [NotMapped]
        public HttpPostedFileBase Picture { get; set; }

        public Boolean Approved { get; set; } = false;

        [Required]
        public string UserId { get; set; }
        public virtual ApplicationUser User {get; set;}
    }
}