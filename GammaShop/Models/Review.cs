﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GammaShop.Models
{
    public class Review
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }

        public string Title { get; set; }

        [Required(ErrorMessage = "Please leave a mark")]
        public float Rating { get; set; }

        [Required(ErrorMessage = "Please tell us your opinion")]
        public string Comment { get; set; }

        public DateTime ListingDate { get; set; }

        public string AuthorId { get; set; }
        public virtual ApplicationUser Author { get; set; }
        public string UserId { get; internal set; }
    }
}