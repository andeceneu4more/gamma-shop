﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GammaShop.Models
{
    public class Order
    {

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "InvoiceId is required")]
        public int InvoiceId { get; set; }
        public virtual Invoice Invoice { get; set; }

        [Required(ErrorMessage = "ProductId is required")]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "The field should be a data type")]
        public DateTime RequestingDate { get; set; }

        public int Quantity { get; set; } = 1;

        public float UnitPrice { get; set; }
    }
}