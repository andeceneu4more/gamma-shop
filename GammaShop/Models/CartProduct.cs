﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GammaShop.Models
{
    [NotMapped]
    public class CartProduct
    {
        [Key]
        public int Id { get; set; }

        public int ProductId { get; set; }

        public string UserId { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "The field should be a data type")]
        public DateTime RequestingDate { get; set; }

        public int Quantity { get; set; }

        public float UnitPrice { get; set; }
    }
}