﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GammaShop.Models
{
    public class Invoice
    {
        [Key]
        public int Id { get; set; }

        public String UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "The field should be a data type")]
        public DateTime RequestingDate { get; set; }

        public Boolean Finished { get; set; } = false;

        public float Bill { get; set; } = 0;
    }
}